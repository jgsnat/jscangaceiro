export class Negociacoes {
  constructor(armadilha) {

    this._negociacoes = [];
    // this._armadilha = armadilha;
    // this._contexto = contexto; 2ª Solução
    Object.freeze(this);
  }

  adiciona(negociacao){
    this._negociacoes.push(negociacao);
    // this._armadilha(this);
    // this._armadilha.call(this._contexto,this); 2ª Solução
  }

  paraArray(){
    return [].concat(this._negociacoes);
  }

  get volumeTotal(){
    /*let total = 0;

    for(let i = 0; i < this._negociacoes.length; i++){
      total+=this._negociacoes[i].volume;
    }
    return total;*/

    return this._negociacoes
      .reduce((total, negociacao) => total + negociacao.volume, 0);
  }

  esvazia(){
    this._negociacoes.length = 0;
    // this._armadilha(this);
    // this._armadilha.call(this._contexto,this); 2ª Solução
  }
}
