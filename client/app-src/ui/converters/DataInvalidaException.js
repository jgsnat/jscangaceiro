import { ApplicationExcepton } from '../../util/ApplicationException.js';

export class DataInvalidaException extends ApplicationExcepton {
    constructor(){
        super('A data deve estar no formato dd/mm/aaaa');
    }
}